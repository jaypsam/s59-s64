const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');



// checkEmailesists function
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
};

// rigisterUser function
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
};

// loginUser function
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			// compareSync(<dataToCompare>, <encryopted password>)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

// userDetails function

module.exports.getProfile = (userData) => {
    return User.findById(userData.id).then(result => {
    		console.log(userData)
            if(result == null) {
              return false

            } else {
            // Terminal
            console.log(result)	
            result.password="******";
            return result
            }
        })
    };


module.exports.order = async (data) => {
     let isUserUpdated = await User.findById(data.userId) 
         .then(user => {
             user.orders.push ({
                 productId: data.productId,
                 productName: data.productName,
                 quantity: data.quantity,
                 totalAmount: data.totalAmount
             })

             return user.save().then((user,error) =>{
             if (error){
                     return false
             } else { 
                	return true
             }
        })
    })

        let isProductUpdated = await Product.findById(data.productId)
         .then(product => { product.order.push({userId : data.userId

         })

            	return product.save().then((product,error) =>{
                 if (error){
                 return false
                 } else { 
                     return true
                 }
             })

     })

         if (isUserUpdated && isProductUpdated){
             return "User successfully ordered" //true
         } else {
             return Promise.reject('Not authorized to access this page');//false
         }
};


module.exports.getUserDetails = (userData) => {
    return User.findById(userData.id)
    .then(result => {
        console.log(userData)
        if(result == null) {
          return false
        }else {
        console.log(result)
        result.password="******";
        return result
        }
    })
};




