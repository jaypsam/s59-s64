const Product = require('../models/Product');
const User = require('../models/User');

// Create a new product
module.exports.addProduct = (reqBody, userData) => {

	return User.findById(userData.userId).then(result => {
		if(userData.isAdmin == false) {
			return "You are not an Admin"
		} else {
			// Create a variable "newProduct" and instantiate a new "Product" object using the mongoose model.
			// Uses the information from the request body to provide all the neceaary information
			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			});

			// Saves the created object to our DB using .save()
			return newProduct.save().then((product, error) => {
				// Product creation failed
				if(error) {
					return false
				// Product creation successful 
				} else {
					return "Product has been added" //true
				}
			})
		}
	})
};

// Controller function for retreiving all products
module.exports.getAllProduct = (data) => {
	if(data.isAdmin) {
 		return Product.find({}).then(result => {
 			return result
 		})
 	} else {
 		return false
 	}
};

//Retrieve All Active Product
module.exports.getAllActive = () => {

	return Product.find({isActive: true}).then(result => {

		return result
	})
};


// Retrieve a Specific Product
module.exports.getProduct = (reqParams) => {
return Product.findById(reqParams.productId).then(result => {
 		return result
 	})
};

// Update specific product

module.exports.updateProduct = (reqParams, reqBody, data) => {
	if (data) {

		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}

		// Find ID
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((updatedProduct, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	} else {
		return Promise.reject('Not authorized to access this page');
	}
};

// Archive specific product

module.exports.archiveProduct = (reqParams, reqBody, data) => {
	if(data) {
		let archivedProduct = {
			isActive: reqBody.isActive
		}
		return Product.findByIdAndUpdate(reqParams.productId,archivedProduct).then((archivedProduct, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return Promise.reject('Not authorized to access this page');
	}
}

// Active product
module.exports.activeProduct = (data) => {

	return Product.findById(data.productId).then((result, err) => {

		if(data.isAdmin === true) {

			result.isActive = true;

			return result.save().then((activeProduct, err) => {

				
				if(err) {

					return false;

				
				} else {

					return true;
				}
			})

		} else {

			
			return false
		}

	})
}



