const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth.js');


// Routes:
// checkEmail route - checks if email is existing from our DB
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// register route - create user in our DB collection
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.get("/details", auth.verify, (req, res) => {
	//decode
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController));

});

router.post('/order', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		res.send("You are logged in as an Admin, Only users can order")
	} else {
		let data = {
			userId: userData.id,
			productId: req.body.productId,
			productName: req.body.productName,
            quantity: req.body.quantity,
            totalAmount: req.body.totalAmount

		}
		userController.order(data).then(resultFromController => res.send(resultFromController)).catch(error => res.send(error));
	}
});

router.get("/details", auth.verify, (req, res)=>{
    
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    userController.getUserDetails({id: userData.id})
    .then(resultFromController => 
        res.send(resultFromController))
});



module.exports = router;

