const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');



// Route for creating a product
router.post("/", auth.verify, (req, res) => {
	//decode
	const userData = auth.decode(req.headers.authorization)
	// console.log(userData)
	productController.addProduct(req.body, {user: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all the product
router.get("/all", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	productController.getAllProduct(userData).then(resultFromController => res.send(resultFromController))
});

// Route for retrieving specific product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId)

	productController.getProduct(req.params).then(resultFromController =>
		res.send(resultFromController))
});


// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
	const isAdminData = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdminData)

	productController.updateProduct(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController)).catch(error => res.send(error))
});

// Route archiving course
router.put("/:productId/archive", auth.verify, (req, res) => {
	const isAdminData = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdminData)

	productController.archiveProduct(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController)).catch(error => res.send(error))
})

// Route product
router.put('/active/:productId', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	courseController.activeCourse(data).then(resultFromController => res.send(resultFromController))
});




module.exports = router;